# README #

加々美直史のPublic Portfolio

### Ponzu学会システム ###

ライフサイエンスの研究者向け学会のために作成した学会情報システム。
演題情報を閲覧できるほか、Facebookのような「いいね」の仕組みを導入。
参加者が6,000人の分子生物学会で2年間運用したほか、他の多数の学会でも採用された。
Ruby on Rails, Javascript (CoffeeScript), Solrなどを利用

* [Ponzuソースコード](https://bitbucket.org/castle104/ponzu/src/master/) -- 学会システムを運用するためのRails Engine
* [Ponzu Demoソースコード](https://bitbucket.org/castle104/ponzudemo/src/open_source/) -- Ponzuを実装したデモアプリ
* [2013年分子生物学会で運用したときのレポート ("RAWで表示"をクリック)](https://bitbucket.org/naofumi/nkportfolio/src/master/likes_report_mbsj2013.pdf)

### CSVからWebに情報を表示するシステム ###

PHPを利用

* [岩井科学社 製品検索システム1 実働サイト](https://www.iwai-chem.co.jp/ddh_jp2/cloud-clone.php)
* [岩井科学社 製品検索システム2 実働サイト](https://www.iwai-chem.co.jp/ddh_jp2/jackson.php)

### 人事情報管理システム (個人の業務のために作成) ###

個人の業務のために人事情報を一箇所にまとめ、組織図を自動的に作成。
異動の情報をもとに、現時点だけでなく、将来や過去の組織図も作成できる。
昇給の履歴なども管理。
Ruby on Rails, Javascript

* [個人情報を伏せた実働サイト](http://people2.castle104.com)
* [デモサイトのソースコード](https://bitbucket.org/naofumi/peoplemanager_demo/)
